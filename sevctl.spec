Name:           sevctl
Version:        0.1.0
Release:        1%{?dist}
Summary:        Administrative utility for AMD SEV

License:        ASL 2.0
URL:            https://github.com/enarx/sevctl
Source0:        %{url}/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:        %{name}-%{version}-vendor.tar.gz

ExclusiveArch:  %{rust_arches}
BuildRequires:  rust-toolset
BuildRequires:  openssl-devel

%description
%{summary}.


%prep
%setup -q -n %{name}-%{version}

%cargo_prep -V 1


%build
%cargo_build


%install
%cargo_install


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}


%changelog
* Wed Apr 14 2021 Connor Kuehl <ckuehl@redhat.com>
- Initial package
